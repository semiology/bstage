## Setup
- Install [python3.7](https://www.python.org/downloads/) if
  necessary. (Do _not_ install it with `brew`.)  You can check if you
  have python3 and the version with the command 
  
      python3 --version

- Install `virtualenvwrapper` as per the [installation instructions.](https://virtualenvwrapper.readthedocs.io/en/latest/install.html)
Don't forget to add this 

      source /usr/local/bin/virtualenvwrapper.sh
    
to your shell startup file, or virtualenvwrapper
will not work in new terminals that you open.


- Create a `python3` virtual environment:

        mkvirtualenv -p python3 backstage_code_challenge
        
 To subsequently use the `backstage_code_challenge` virtual environment, simply type `workon backstage_code_challenge`.
 Your prompt should now be "(backstage_code_challenge)"

- Go to the django_root directory and install Python requirements in the virtual environment:

        pip install -r requirements.txt
        
 - From the Django root directory, initialize the database and data:
 
        ./manage.py migrate
        ./manage.py loaddata calculations/fixtures/calculators.json
 
 
## Running the Code Challenge
 - Ensure that you are using the python3 virtualenv
 
        workon backstage_code_challenge
 
 - Go to the django_root directory on the command line
 
 - start the Django server from the commmand line like 
 
         ./manage.py runserver
         
 - From a browser, the JSON endpoint will now be available at http://localhost:8000/calculate/squares_difference/?n=100
 (you can change the value of n) and the form page will be available at http://localhost:8000/ -- please use a "modern" browser for the form.
