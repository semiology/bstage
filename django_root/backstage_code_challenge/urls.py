from django.contrib import admin
from django.urls import path
from django.views.generic import TemplateView

from calculations import views as calculation_views


urlpatterns = [
    path("admin/", admin.site.urls),
    path("calculate/<str:calculator_name>/", calculation_views.CalculatorJsonView.as_view(), name="calculate"),
    path("", TemplateView.as_view(template_name='home.html'), name="home")
]
