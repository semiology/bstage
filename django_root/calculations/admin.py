from django.contrib import admin

from .models import CalculationCounter, Calculator


admin.site.register(CalculationCounter)
admin.site.register(Calculator)
