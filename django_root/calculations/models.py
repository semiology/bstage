import importlib

from django.db import models, transaction


class Calculator(models.Model):
    """
    Model mapping a name to a calculation method, to allow
    new URLs to be added through data.
    """
    name = models.CharField(
        primary_key=True,
        max_length=20,
        )
    
    calculation_method = models.CharField(max_length=50, help_text="dot path to calculation method")
    
    def __str__(self):
        return self.name
    
    def calculate(self, **args):
        """
        Returns the object's calculation method with the given arguments
        """
        #  NOTE: this is a potential huge security hole, but illustrates the idea of mapping
        # calculation methods to URLs in data.
        mod_name, func_name = self.calculation_method.rsplit('.',1)
        assert mod_name, "calculations.calculators"  # Limit arbitrary code execution
        mod = importlib.import_module(mod_name)
        func = getattr(mod, func_name)
        return func(**args)
                

class CalculationCounterManager(models.Manager):
    def increment_counter(self, calculator, arguments: str):
        """
        Increments the value of count for counter object identified uniquely
        by calculator and arguments.
        Creates new objects if needed while avoiding race conditions.
        
        The awkward constuct is a workaround for
        https://code.djangoproject.com/ticket/25195
        """
        # NOTE: select_for_update isn't supported in sqlite, but you would never use sqlite
        # for production anyway.
        
        with transaction.atomic():
            counter, _created = self.select_for_update() \
                .get_or_create(calculator=calculator, arguments=arguments)
            counter.count += 1
            counter.save()
            return counter
            

class CalculationCounter(models.Model):   
    calculator = models.ForeignKey(Calculator, on_delete=models.CASCADE)
    count = models.PositiveIntegerField(
        default=0, 
        help_text="the number of times operation run for arguments",
        )
    arguments = models.CharField(
        help_text="represention of argument(s) used with calculator",
        max_length=100
        )
    
    objects = CalculationCounterManager()
    
    class Meta:
        unique_together = ["calculator", "arguments"]
    
    def __str__(self):
        return f"{self.calculator} ({self.arguments}):{self.count}"
    
    @staticmethod
    def cannonize_arguments(arguments_as_dict: dict) -> str:
        """
        Given a dictionary of one or more arguments, returns a
        canonical representation as a string.
        """
        sorted_keys = sorted(arguments_as_dict.keys())
        return ",".join([f"{key}={arguments_as_dict[key]}" for key in sorted_keys])
