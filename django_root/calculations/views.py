from django.http import Http404, JsonResponse
from django.utils import timezone
from django.views.generic.base import View

from .models import CalculationCounter, Calculator


class CalculatorJsonView(View):
    """
    Provides a formatted JSON response for calculated data.
    """
    
    def get_payload(self, calculator: Calculator, arguments: int):
        counter = CalculationCounter.objects.increment_counter(calculator, CalculationCounter.cannonize_arguments(arguments))
        occurrences = counter.count
        
        # Map arguments to integers
        try:
            arguments = {key: int(arguments[key]) for key in arguments.keys()}
        except ValueError:
            return JsonResponse({"error": "bad argument(s)"})

        # Get and return solution
        solution = calculator.calculate(**arguments)
        payload_dict = {
            "datetime": timezone.now(),
            "value": solution,
            "number": arguments["n"], # This would need to modified for multiple arguments
            "occurrences": occurrences
            }
        return payload_dict

    def get(self, request, calculator_name, *args, **kwargs):
        try:
            calculator = Calculator.objects.get(name=calculator_name)
        except Calculator.DoesNotExist:
            raise Http404("Calculation operation does not exist")

        try:
            payload = self.get_payload(calculator, request.GET)
        except AssertionError:
            return JsonResponse({"error": "argument(s) out of range"})
        return JsonResponse(payload)
