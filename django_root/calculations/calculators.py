def sum_of_squares(n: int) -> int:
    """
    The sum of the squares of the first n natural numbers.
    """
    return sum([n * n for n in range(1, n+1)])


def square_of_sum(n: int) -> int:
    """
    The square of the sum of the first n natural numbers.
    """
    return ( n * (n + 1) // 2) ** 2


def squares_difference(n: int) -> int:
    """
    The difference between the square of sum
    and the sum of squares.
    """
    assert type(n) == int, "argument must be integer"
    assert n > 0 and n <= 100, "argument out of range"
    return square_of_sum(n) - sum_of_squares(n)
