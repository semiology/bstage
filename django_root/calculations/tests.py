import json

from django.http.response import JsonResponse
from django.test import RequestFactory, TestCase
from django.urls import reverse

from .calculators import square_of_sum, squares_difference, sum_of_squares
from .views import CalculatorJsonView


class CalculatorTests(TestCase):
    """
    Functional tests.
    """
    def test_sum_of_squares(self):
        self.assertEqual(sum_of_squares(10), 385)
        self.assertEqual(sum_of_squares(100), 338350)
    
    def test_square_of_sum(self):
        self.assertEqual(square_of_sum(10), 3025)
        self.assertEqual(square_of_sum(100), 25502500)
    
    def test_squares_difference(self):
        self.assertEqual(squares_difference(10), 2640)
        self.assertEqual(squares_difference(2), 4)
        self.assertEqual(squares_difference(3), 22)
        with self.assertRaises(AssertionError):
            squares_difference("fish")
        with self.assertRaises(AssertionError):
            squares_difference(0)
        with self.assertRaises(AssertionError):
            squares_difference(200)


class APITests(TestCase):
    """
    API Tests
    """
    fixtures = ("calculators.json",)
    
    def test_squares_difference_url(self):
        number = 10
        calculator_name = "squares_difference"
        request_factory = RequestFactory()
        url = f'{reverse("calculate", args=[calculator_name])}?n={number}'
        request = request_factory.get(url)

        response = CalculatorJsonView.as_view()(request, calculator_name)
        self.assertEqual(JsonResponse, type(response))
        output_dict = json.loads(response.content)

        self.assertEqual(output_dict["number"], number)
        self.assertEqual(output_dict["value"], 2640)
        self.assertEqual(output_dict["occurrences"], 1)
    
        response = CalculatorJsonView.as_view()(request, calculator_name)
        output_dict = json.loads(response.content)
        self.assertEqual(output_dict["occurrences"], 2)
